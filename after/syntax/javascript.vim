if starmatt#should_abort('javascript', 'javascript.jsx')
    finish
endif

hi! link javaScriptNumber           Constant
hi! link javaScriptNull             Constant
hi! link javaScriptFunction         Function

hi! link jsArrowFunction            Function
hi! link jsClassMethodType          Keyword
hi! link jsDestructuringAssignment  PreProc
hi! link jsDocParam                 PreProc
hi! link jsDocTags                  Keyword
hi! link jsDocType                  Type
hi! link jsDocTypeBrackets          Constant
hi! link jsFuncArgOperator          Operator
hi! link jsFunction                 Function
hi! link jsNull                     Constant
hi! link jsTemplateBraces           Special
hi! link jsThis                     Special
hi! link jsUndefined                Constant
hi! link jsComma                    Noise
hi! link jsSemicolon                Noise
hi! link jsBuiltinObjects           Constant
hi! link jsDot                      Type
hi! link jsObjectKey                Function
hi! link jsObjectColon              Type
