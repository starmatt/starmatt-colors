if starmatt#should_abort('css')
    finish
endif

hi! link cssAttrComma         Delimiter
hi! link cssAttrRegion        Special
hi! link cssAttributeSelector Constant
hi! link cssBraces            Delimiter
hi! link cssFunctionComma     Delimiter
hi! link cssNoise             Special
hi! link cssProp              Identifier
hi! link cssPseudoClass       Delimiter
hi! link cssPseudoClassId     Constant
hi! link cssUnitDecorators    Type
hi! link cssVendor            Constant
