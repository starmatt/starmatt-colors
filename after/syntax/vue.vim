if starmatt#should_abort('vue')
    finish
endif

syntax match htmlArg contained "\<[-0-9A-Za-z_]\+\>"

syntax clear htmlH1
syntax clear htmlH2
syntax clear htmlH3
syntax clear htmlH4
syntax clear htmlH5
syntax clear htmlH6
syntax clear htmlLink

hi! link VueQuote       Special
hi! link VueBrace       Special
hi! link VueKey         htmlArg


" VueComponentName xxx links to htmlTagName
" VueKey         xxx links to Type
" VueQuote       xxx links to Constant
" VueAttr        xxx links to htmlTag
" VueValue       xxx links to None
" VueInject      xxx links to Constant
" VueBrace       xxx links to Constant
" VueExpression  xxx links to Normal

