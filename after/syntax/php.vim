if starmatt#should_abort('php')
    finish
endif

hi! link phpClass           Type
hi! link phpClasses         Type
hi! link phpDocTags         Comment
hi! link phpFunction        Function
hi! link phpSpecialFunction Special
hi! link phpNullValue       Constant
