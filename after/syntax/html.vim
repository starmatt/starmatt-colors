if starmatt#should_abort('html')
    finish
endif

syntax clear htmlH1
syntax clear htmlH2
syntax clear htmlH3
syntax clear htmlH4
syntax clear htmlH5
syntax clear htmlH6
syntax clear htmlLink

hi! link htmlTag            Comment
hi! link htmlEndTag         Comment
hi! link htmlTagName        Function
hi! link htmlSpecialTagName Statement
hi! link htmlArg            Type
hi! link htmlTitle          Statement
hi! link htmlLink           Underlined
hi! link htmlSpecialChar    Special
