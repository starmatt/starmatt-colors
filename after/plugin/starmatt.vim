" fzf
if exists('g:loaded_fzf') && ! exists('g:fzf_colors')
    let g:fzf_colors = {
        \ 'fg':         ['fg', '_Gray'],
        \ 'fg+':        ['fg', 'Normal'],
        \ 'bg':         ['bg', 'Normal'],
        \ 'bg+':        ['bg', 'Normal'],
        \ 'hl':         ['fg', '_Green'],
        \ 'hl+':        ['fg', '_Green'],
        \ 'info':       ['fg', '_Yellow'],
        \ 'border':     ['fg', '_Gray'],
        \ 'prompt':     ['fg', '_Blue'],
        \ 'pointer':    ['fg', '_Blue'],
        \ 'marker':     ['fg', '_Green'],
        \ 'spinner':    ['fg', '_Yellow'],
        \ 'header':     ['fg', '_Gray']
        \ }
endif

" ALE
if exists('g:ale_enabled')
    hi! link ALEError                   _Underline
    hi! link ALEWarning                 _Underline
    hi! link ALEInfo                    _Underline
    hi! link ALESignColumnWithoutErrors Normal

    hi! link ALEErrorSign               _Red
    hi! link ALEWarningSign             _Yellow
    hi! link ALEInfoSign                _Blue

    hi! link ALEVirtualTextError        _Gray
    hi! link ALEVirtualTextWarning      _Gray
endif
