scriptencoding utf-8

" Helpers

" Takes a foreground color name, background color name, and optionally one or
" more attr-list items as input, transforms it to the format accepted by
" airline#themes#generate_color_map and returns that value
func! s:clr(fg, bg, ...)
    let l:fg = g:starmatt#palette[a:fg]
    let l:bg = g:starmatt#palette[a:bg]
    return [ l:fg[0], l:bg[0], l:fg[1], l:bg[1] ] +
        \ filter(copy(a:000), 'type(v:val) == 1 && len(v:val) > 0')
endfunc

" Airline theme
let g:airline#themes#starmatt#palette = {}

" NORMAL MODE
" -----------
let g:airline#themes#starmatt#palette.normal = airline#themes#generate_color_map(
    \ s:clr('black', 'green'),
    \ s:clr('green', 'gray'),
    \ s:clr('green', 'black')
    \ )

let g:airline#themes#starmatt#palette.normal_modified = {
    \ 'airline_c': s:clr('white', 'black')
    \ }

let g:airline#themes#starmatt#palette.normal.airline_error =
    \ s:clr('red', 'black')

let g:airline#themes#starmatt#palette.normal_modified.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.normal.airline_warning =
    \ s:clr('yellow', 'black')

let g:airline#themes#starmatt#palette.normal_modified.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning


" INSERT MODE
" -----------
let g:airline#themes#starmatt#palette.insert = airline#themes#generate_color_map(
    \ s:clr('white', 'blue'),
    \ s:clr('blue', 'gray'),
    \ s:clr('blue', 'black'),
    \ )

let g:airline#themes#starmatt#palette.insert_modified = g:airline#themes#starmatt#palette.normal_modified

let g:airline#themes#starmatt#palette.insert_paste = {
    \ 'airline_a': s:clr('black', 'red')
    \ }

let g:airline#themes#starmatt#palette.insert.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.insert_modified.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.insert.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning

let g:airline#themes#starmatt#palette.insert_modified.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning


" REPLACE MODE
" ------------
let g:airline#themes#starmatt#palette.replace =
    \ copy(g:airline#themes#starmatt#palette.insert)

let g:airline#themes#starmatt#palette.replace.airline_a =
    \ s:clr('white', 'red')

let g:airline#themes#starmatt#palette.replace_modified =
    \ g:airline#themes#starmatt#palette.normal_modified

let g:airline#themes#starmatt#palette.replace.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.replace_modified.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.replace.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning

let g:airline#themes#starmatt#palette.replace_modified.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning


" VISUAL MODE
" -----------
let g:airline#themes#starmatt#palette.visual = airline#themes#generate_color_map(
    \ s:clr('black', 'yellow'),
    \ s:clr('yellow', 'gray'),
    \ s:clr('yellow', 'black')
    \ )

let g:airline#themes#starmatt#palette.visual_modified =
    \ g:airline#themes#starmatt#palette.normal_modified

let g:airline#themes#starmatt#palette.visual.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.visual_modified.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.visual.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning

let g:airline#themes#starmatt#palette.visual_modified.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning


" INACTIVE
" --------
let g:airline#themes#starmatt#palette.inactive = airline#themes#generate_color_map(
    \ s:clr('gray', 'black'),
    \ s:clr('black', 'gray'),
    \ s:clr('gray', 'black')
    \ )

let g:airline#themes#starmatt#palette.inactive_modified =
    \ g:airline#themes#starmatt#palette.normal_modified

let g:airline#themes#starmatt#palette.inactive.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.inactive_modified.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.inactive.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning

let g:airline#themes#starmatt#palette.inactive_modified.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning


" COMMAND MODE
" ------------
let g:airline#themes#starmatt#palette.commandline = airline#themes#generate_color_map(
    \ s:clr('white', 'red'),
    \ s:clr('red', 'gray'),
    \ s:clr('red', 'black')
    \ )

let g:airline#themes#starmatt#palette.commandline_modified =
    \ g:airline#themes#starmatt#palette.normal_modified

let g:airline#themes#starmatt#palette.commandline.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.commandline_modified.airline_error =
    \ g:airline#themes#starmatt#palette.normal.airline_error

let g:airline#themes#starmatt#palette.commandline.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning

let g:airline#themes#starmatt#palette.commandline_modified.airline_warning =
    \ g:airline#themes#starmatt#palette.normal.airline_warning


" ACCENTS
" -------
let g:airline#themes#starmatt#palette.accents = {
    \ 'red': s:clr('red', 'black'),
    \ }

" TABLINE
" -------
let g:airline#themes#starmatt#palette.tabline = {
    \ 'airline_tabfill': s:clr('black', 'black'),
    \
    \ 'airline_tablabel': s:clr('black', 'green'),
    \ 'airline_tabtype': s:clr('yellow', 'black'),
    \
    \ 'airline_tab': s:clr('gray', 'black'),
    \ 'airline_tabsel': s:clr('green', 'gray'),
    \ 'airline_tabmod': s:clr('white', 'gray'),
    \ 'airline_tabhid': s:clr('gray', 'black'),
    \
    \ 'airline_tab_right': s:clr('gray', 'black'),
    \ 'airline_tabsel_right': s:clr('green', 'gray'),
    \ 'airline_tabmod_right': s:clr('white', 'gray'),
    \ 'airline_tabhid_right': s:clr('gray', 'black'),
    \ }
