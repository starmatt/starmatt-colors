" Palette
let g:starmatt#palette           = {}

let g:starmatt#palette.black     = ['#161a1d',    000]
let g:starmatt#palette.red       = ['#ff6666',    001]
let g:starmatt#palette.green     = ['#66ff99',    002]
let g:starmatt#palette.yellow    = ['#f7ec6e',    003]
let g:starmatt#palette.blue      = ['#5989eb',    004]
let g:starmatt#palette.magenta   = ['#bd93f9',    005]
let g:starmatt#palette.cyan      = ['#7fbfff',    006]
let g:starmatt#palette.white     = ['#e7ecef',    007]
let g:starmatt#palette.gray      = ['#414d58',    008]
let g:starmatt#palette.pink      = ['#e6b3e6',    015]

let g:starmatt#palette.fg        = g:starmatt#palette.white
let g:starmatt#palette.bg        = ['#21262c', 'NONE']

" Helper function that takes a variadic list of filetypes as args and returns
" whether or not the execution of the ftplugin should be aborted.
func! starmatt#should_abort(...)
    if !exists('g:colors_name') || g:colors_name !=# 'starmatt'
        return 1
    elseif a:0 > 0 && (!exists('b:current_syntax') || index(a:000, b:current_syntax) == -1)
        return 1
    endif

    return 0
endfunction

" Helper function to create highlight statements
func! starmatt#hl(scope, fg, bg, attr_list)
    let l:fg = copy(a:fg)
    let l:bg = copy(a:bg)
    let l:attrs = len(a:attr_list) > 0 ? join(a:attr_list, ',') : 'NONE'

    let l:hl_string = [
        \ 'highlight', a:scope,
        \ 'guifg=' . l:fg[0], 'ctermfg=' . l:fg[1],
        \ 'guibg=' . l:bg[0], 'ctermbg=' . l:bg[1],
        \ 'gui=' . l:attrs, 'cterm=' . l:attrs,
        \ ]

    exec join(l:hl_string, ' ')
endfunction
