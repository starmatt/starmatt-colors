" Custom Vim colorscheme

scriptencoding utf-8

hi clear
let g:colors_name = "starmatt"
set background=dark

" Palette
let s:fg        = g:starmatt#palette.fg
let s:bg        = g:starmatt#palette.bg

let s:black     = g:starmatt#palette.black
let s:gray      = g:starmatt#palette.gray
let s:red       = g:starmatt#palette.red
let s:green     = g:starmatt#palette.green
let s:yellow    = g:starmatt#palette.yellow
let s:blue      = g:starmatt#palette.blue
let s:magenta   = g:starmatt#palette.magenta
let s:cyan      = g:starmatt#palette.cyan
let s:pink      = g:starmatt#palette.pink

let s:none      = ['NONE', 'NONE']

" Hightlight groups
call starmatt#hl('Normal', s:fg, s:bg, [])
call starmatt#hl('StatusLine', s:fg, s:black, [])
call starmatt#hl('StatusLineNC', s:gray, s:black, [])
call starmatt#hl('StatusLineTerm', s:black, s:green, [])
call starmatt#hl('StatusLineTermNC', s:fg, s:black, [])
call starmatt#hl('CursorLine', s:none, s:black, [])
call starmatt#hl('CursorLineNr', s:fg, s:black, [])
call starmatt#hl('MatchParen', s:magenta, s:green, ['underline'])

" Custom highlight groups
call starmatt#hl('_Black', s:black, s:none, [])
call starmatt#hl('_BlackBg', s:fg, s:black, [])
call starmatt#hl('_BlackSubtle', s:gray, s:black, [])

call starmatt#hl('_Gray', s:gray, s:none, [])
call starmatt#hl('_GrayBg', s:black, s:gray, [])
call starmatt#hl('_GraySubtle', s:fg, s:gray, [])

call starmatt#hl('_Red', s:red, s:none, [])
call starmatt#hl('_RedBg', s:fg, s:red, [])
call starmatt#hl('_RedSubtle', s:red, s:black, [])
call starmatt#hl('_RedFocus', s:red, s:gray, [])

call starmatt#hl('_Green', s:green, s:none, [])
call starmatt#hl('_GreenBg', s:black, s:green, [])
call starmatt#hl('_GreenSubtle', s:green, s:black, [])
call starmatt#hl('_GreenFocus', s:green, s:gray, [])

call starmatt#hl('_Yellow', s:yellow, s:none, [])
call starmatt#hl('_YellowBg', s:black, s:yellow, [])
call starmatt#hl('_YellowSubtle', s:yellow, s:black, [])
call starmatt#hl('_YellowFocus', s:yellow, s:gray, [])

call starmatt#hl('_Blue', s:blue, s:none, [])
call starmatt#hl('_BlueBg', s:black, s:blue, [])
call starmatt#hl('_BlueSubtle', s:blue, s:black, [])
call starmatt#hl('_BlueFocus', s:blue, s:gray, [])

call starmatt#hl('_Magenta', s:magenta, s:none, [])
call starmatt#hl('_MagentaBg', s:fg, s:magenta, [])
call starmatt#hl('_MagentaSubtle', s:magenta, s:black, [])
call starmatt#hl('_MagentaFocus', s:magenta, s:gray, [])

call starmatt#hl('_Cyan', s:cyan, s:none, [])
call starmatt#hl('_CyanBg', s:black, s:cyan, [])
call starmatt#hl('_CyanSubtle', s:cyan, s:black, [])
call starmatt#hl('_CyanFocus', s:cyan, s:gray, [])

call starmatt#hl('_Pink', s:pink, s:none, [])
call starmatt#hl('_PinkBg', s:black, s:pink, [])
call starmatt#hl('_PinkSubtle', s:pink, s:black, [])
call starmatt#hl('_PinkFocus', s:pink, s:gray, [])

call starmatt#hl('_Link', s:yellow, s:none, ['underline'])
call starmatt#hl('_Reverse', s:none, s:none, ['reverse'])
call starmatt#hl('_Underline', s:none, s:none, ['underline'])

" Interface
hi! link ColorColumn            _BlackBg
hi! link CursorColumn           CursorLine
hi! link DiffAdd                _GreenFocus
hi! link DiffAdded              _GreenFocus
hi! link DiffChange             _CyanFocus
hi! link DiffDelete             _RedFocus
hi! link DiffRemoved            _RedFocus
hi! link DiffText               _YellowFocus
hi! link Directory              _Blue
hi! link EndOfBuffer            _Gray
hi! link ErrorMsg               _RedBg
hi! link FoldColumn             Normal
hi! link Folded                 Normal
hi! link IncSearch              _Reverse
hi! link HighlightedYankRegion  _GreenFocus
hi! link LineNr                 _Gray
hi! link MoreMsg                _Green
hi! link NonText                _Pink
hi! link Pmenu                  _BlackBg
hi! link PmenuSel               _GreenFocus
hi! link PmenuSbar              _BlackBg
hi! link PmenuThumb             _GrayBg
hi! link Question               _Yellow
hi! link RedrawDebugClear       _YellowBg
hi! link RedrawDebugComposed    _GreenBg
hi! link RedrawDebugRecompose   _RedBg
hi! link Search                 _Reverse
hi! link SignColumn             Normal
hi! link SpecialKey             _Pink
hi! link TabLine                _GreenBg
hi! link TabLineFill            _BlackBg
hi! link TabLineSel             _GreenBg
hi! link Title                  _Magenta
hi! link VertSplit              _Black
hi! link Visual                 _Reverse
hi! link VisualNOS              Visual
hi! link WarningMsg             _Red
hi! link Whitespace             _RedBg
hi! link ExtraWhitespace        _RedBg

" Syntax
hi! link Error                  _RedBg

hi! link Comment                _Gray
hi! link Underlined             _Magenta
hi! link Todo                   _GraySubtle

hi! link Constant               _Green
hi! link String                 _Green
hi! link Character              _Green
hi! link Number                 _Green
hi! link Boolean                _Green
hi! link Float                  _Green

hi! link Function               _Cyan
hi! link Delimiter              _Pink
hi! link Identifier             _Cyan
hi! link Type	                _Blue

hi! link Statement              _Magenta
hi! link Conditional            _Magenta
hi! link Repeat                 _Magenta
hi! link Label                  _Magenta
hi! link Operator               _Blue
hi! link Keyword                _Magenta
hi! link Exception              _Red

hi! link PreProc                _Red
hi! link Include                _Red
hi! link Define                 _Red
hi! link Macro                  _Red
hi! link PreCondit              _Red
hi! link Structure              _Red
hi! link Typedef                _Red

hi! link Special                _Pink
hi! link SpecialComment         _Yellow
hi! link Tag                    _Yellow
hi! link helpHyperTextJump      _Link
hi! link helpCommand            _Magenta
hi! link helpExample            _Green
hi! link helpBacktick           Special
hi! link Noise                  Special

" Nvim
hi! link NvimInternalError      _RedBg
